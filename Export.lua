package.path  = package.path .. ";.\\LuaSocket\\?.lua"
package.cpath = package.cpath .. ";.\\LuaSocket\\?.dll"

local exportSettings = {}
exportSettings.appAddress = "127.0.0.1"
exportSettings.port = 20084
exportSettings.twsPeriod = 0.3
exportSettings.version = 1

local exportHook = {}
exportHook.LuaExportBeforeNextFrame = LuaExportBeforeNextFrame
exportHook.LuaExportActivityNextEvent = LuaExportActivityNextEvent

local socket = require("socket")
local connection = {}
connection = socket.udp()
connection:settimeout(0)
connection:setpeername(exportSettings.appAddress, exportSettings.port)

local json = loadfile("Scripts\\JSON.lua")()

LuaExportActivityNextEvent = function(currentTime)
	local nextTime = currentTime + exportSettings.twsPeriod

	local sendData = {}
	sendData.version = exportSettings.version
	sendData.twsParams = CollectTWSParameters()
	socket.try(connection:send(json:encode_pretty(sendData)))

	pcall(function()
		if exportHook.LuaExportActivityNextEvent then
			exportHook.LuaExportActivityNextEvent(currentTime)
		end

	end)
	return nextTime
end

LuaExportBeforeNextFrame = function()
	local sendData = {}
	sendData.version = exportSettings.version
	sendData.planeParams = CollectPlaneParameters()
	socket.try(connection:send(json:encode_pretty(sendData)))

	pcall(function()
		if exportHook.LuaExportBeforeNextFrame then
			exportHook.LuaExportBeforeNextFrame()
		end
	end)
end

function LuaExportStop()
end

function CollectTWSParameters()
	local tswInfo = {}
	tswInfo.emitters = {}
	local rawTwsInfo = LoGetTWSInfo()

	if rawTwsInfo == nil then
		return tswInfo
	end

	for emitterIndex = 1, #rawTwsInfo.Emitters, 1 do
		local rawEmitter = rawTwsInfo.Emitters[emitterIndex]
		local emitter = {}
		local emitterObject = LoGetObjectById(rawEmitter.ID)
		emitter.type = rawEmitter.Type
		emitter.name = LoGetNameByType(emitter.type.level1, emitter.type.level2, emitter.type.level3, emitter.type.level4)
		emitter.signalType = rawEmitter.SignalType
		emitter.alt = 0
		if (emitterObject) then
			emitter.alt = emitterObject.LatLongAlt.Alt or 0
		end
		emitter.azimuth = rawEmitter.Azimuth * 57.3
		emitter.power = rawEmitter.Power
		emitter.priority = rawEmitter.Priority

		table.insert(tswInfo.emitters, emitter)
	end

	return tswInfo
end

function Lerp(min, max, t)
	return min + t * (max - min)
end

function ArrayLerpInerpolator(baseValues, realValues, t)
	if (t <= baseValues[1]) then
		return baseValues[1]
	end

	if (t >= baseValues[#baseValues]) then
		return baseValues[#baseValues]
	end

	local minIndex = 1
	local maxIndex = #baseValues

	while minIndex <= maxIndex do
		local mid = math.floor((maxIndex + minIndex) / 2 + 0.5)
		if (t <= baseValues[mid]) then
			maxIndex = mid - 1;
		else
			minIndex = mid + 1;
		end
	end

	local scaleT = (t - baseValues[minIndex - 1]) / (baseValues[minIndex] - baseValues[minIndex - 1])

	return Lerp(realValues[minIndex - 1], realValues[minIndex], scaleT)
end

local additionalPlaneParametersCollectors = {
	["L-39C"] = function(planeParameters)
		local mainPanel = GetDevice(0)
		if type(mainPanel) == 'table' then
			mainPanel:update_arguments()

			if (planeParameters.slipBallPosition == nil) then
				planeParameters.slipBallPosition = mainPanel:get_argument_value(40)
			end

			if (planeParameters.brake == nil) then
				planeParameters.brake = mainPanel:get_argument_value(117)
			end

			if (planeParameters.fuel == nil) then
				planeParameters.fuel = ArrayLerpInerpolator({0.0, 0.127, 0.239, 0.35, 0.458, 0.56, 0.685, 0.82, 1.0}, {0.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 825.0}, mainPanel:get_argument_value(91))
			end

			if (planeParameters.rpmL == nil) then
				planeParameters.rpmL = ArrayLerpInerpolator({0.0, 0.09, 0.18, 0.28, 0.372, 0.468, 0.555, 0.645, 0.733, 0.822, 0.909, 1.0}, {0.0, 10.0, 20.0, 30.0, 40.0,  50.0,  60.0, 70.0, 80.0, 90.0, 100.0, 110.0}, mainPanel:get_argument_value(84))
			end

			if (planeParameters.tempL == nil) then
				planeParameters.tempL = Lerp(0.0, 900.0, mainPanel:get_argument_value(90))
			end

			if (planeParameters.pressure == nil) then
				planeParameters.pressure = Lerp(670.0, 826.0, mainPanel:get_argument_value(56))
			end
		end
	end,
	["MiG-15bis"] = function(planeParameters)
		local mainPanel = GetDevice(0)
		if type(mainPanel) == 'table' then
			mainPanel:update_arguments()

			if (planeParameters.slipBallPosition == nil) then
				planeParameters.slipBallPosition = mainPanel:get_argument_value(8)
			end

			if (planeParameters.fuel == nil) then
				planeParameters.fuel = ArrayLerpInerpolator({0.0, 0.047, 0.136, 0.22, 0.38, 0.52, 0.631, 0.755, 0.869, 0.921, 1.0}, {-100.0, 0.0, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0, 700.0, 800.0, 1050.0}, mainPanel:get_argument_value(47))
			end

			if (planeParameters.tempL == nil) then
				planeParameters.tempL = Lerp(300.0, 900.0, mainPanel:get_argument_value(41))
			end

			if (planeParameters.rpmL == nil) then
				planeParameters.rpmL = Lerp(300.0, 900.0, mainPanel:get_argument_value(50))
			end
		end
	end,
	["MiG-21Bis"] = function(planeParameters)
		local mainPanel = GetDevice(0)
		if type(mainPanel) == 'table' then
			mainPanel:update_arguments()

			if (planeParameters.slipBallPosition == nil) then
				planeParameters.slipBallPosition = mainPanel:get_argument_value(31)
			end

			if (planeParameters.rpmL == nil) then
				planeParameters.rpmL =  Lerp(0.0, 110.0, mainPanel:get_argument_value(50))
			end

			if (planeParameters.rpmR == nil) then
				planeParameters.rpmR =  Lerp(0.0, 110.0, mainPanel:get_argument_value(670))
			end
		end
	end,
	["Mi-8MT"] = function(planeParameters)
		local mainPanel = GetDevice(0)
		if type(mainPanel) == 'table' then
			mainPanel:update_arguments()

			if (planeParameters.rpmL == nil) then
				planeParameters.rpmL =  Lerp(0.0, 110.0, mainPanel:get_argument_value(40))
			end

			if (planeParameters.rpmR == nil) then
				planeParameters.rpmR =  Lerp(0.0, 110.0, mainPanel:get_argument_value(41))
			end

			if (planeParameters.tempL == nil) then
				planeParameters.tempL = Lerp(0.0, 1200.0, mainPanel:get_argument_value(43))
			end

			if (planeParameters.tempR == nil) then
				planeParameters.tempR = Lerp(0.0, 1200.0, mainPanel:get_argument_value(45))
			end
		end
	end
}

function CollectPlaneParameters()
	local planeParameters = {}
	selfData = LoGetSelfData()
	if selfData == nil then
		return planeParameters
	end

	planeParameters.planeTypeName = selfData.Name;

	local altitude = LoGetAltitudeAboveSeaLevel()
	if altitude then
		planeParameters.altitude = altitude;
	end

	local ias = LoGetIndicatedAirSpeed()
	if ias then
		planeParameters.airSpeed = ias * 3.6;
	end

	local tas = LoGetTrueAirSpeed()
	if tas then
		planeParameters.tas = tas * 3.6;
	end

	local engineInfo = LoGetEngineInfo();
	if engineInfo then
		local fuelInternal = engineInfo.fuel_internal or 0
		local fuelExternal = engineInfo.fuel_external or 0
		planeParameters.fuel = fuelInternal + fuelExternal

		local rpm = engineInfo.RPM
		if (rpm) then
			planeParameters.rpmL = rpm.left or 0
			planeParameters.rpmR = rpm.right or 0
		end

		local temperature = engineInfo.Temperature
		if (rpm) then
			planeParameters.tempL = temperature.left or 0
			planeParameters.tempR = temperature.right or 0
		end
	end

	local pitch, bank, yaw = LoGetADIPitchBankYaw()
	if pitch then
		planeParameters.pitch = pitch * 57.3;
	end
	if bank then
		planeParameters.roll = bank * 57.3;
	end
	if yaw then
		planeParameters.yaw = yaw * 57.3;
	end

	local vvel = LoGetVerticalVelocity()
	if vvel then
		planeParameters.verticalSpeed = vvel;
	end

	local planePos = selfData.LatLongAlt
	if planePos then
		planeParameters.position = planePos;
	end

	local aoa = LoGetAngleOfAttack()
	if aoa then
		planeParameters.aoa = aoa;
	end

	local machNumber = LoGetMachNumber()
	if machNumber then
		planeParameters.machNumber = machNumber;
	end

	local pressure = LoGetBasicAtmospherePressure()
	if pressure then
		planeParameters.pressure = pressure;
	end

	local accelerationUnits = LoGetAccelerationUnits()
	if accelerationUnits then
		planeParameters.g = accelerationUnits.y or 0
	end

	local slipBallPosition = LoGetSlipBallPosition()
	if slipBallPosition then
		planeParameters.slipBallPosition = slipBallPosition
	end

	local mechInfo = LoGetMechInfo()
	if mechInfo then
		if (mechInfo.flaps.status) then
			planeParameters.flaps = mechInfo.flaps.status
		end

		if (mechInfo.speedbrakes.value) then
			planeParameters.brake = mechInfo.speedbrakes.value
		end

		if (mechInfo.gear.value) then
			planeParameters.gear = mechInfo.gear.value
		end
	end

	-- local angleOfSideSlip = LoGetAngleOfSideSlip()
	-- if angleOfSideSlip then
	-- 	planeParameters.angleOfSideSlip = angleOfSideSlip*57.3
	-- end

	local additionalPlaneParametersCollector = additionalPlaneParametersCollectors[selfData.Name]
	if (additionalPlaneParametersCollector) then
		additionalPlaneParametersCollector(planeParameters)
	end

	return planeParameters;
end
